import {
  CardContent,
  Grid,
} from "@material-ui/core";
import MuiButton from "@material-ui/core/Button";

const Button = (props: any) => {
  return (
    <Grid item>
      <MuiButton {...props} />
    </Grid>
  );
};

const NumericKeyboard = (props: any) => {
  const { formarTexto } = props;
  return (
    <CardContent>
      <Grid
        container
        spacing={2}
        style={{ textAlign: "center" }}
        
      >
        <Button
          onClick={() => {
            formarTexto("7");
          }}
          variant="contained"
          size="large"
        >
          7
        </Button>
        <Button
          onClick={() => {
            formarTexto("8");
          }}
          variant="contained"
          size="large"
        >
          8
        </Button>
        <Button
          onClick={() => {
            formarTexto("9");
          }}
          variant="contained"
          size="large"
        >
          9
        </Button>
      </Grid>
      <Grid
        container
        spacing={2}
        style={{ textAlign: "center" }}
        
      >
        <Button
          onClick={() => {
            formarTexto("4");
          }}
          variant="contained"
          size="large"
        >
          4
        </Button>
        <Button
          onClick={() => {
            formarTexto("5");
          }}
          variant="contained"
          size="large"
        >
          5
        </Button>
        <Button
          onClick={() => {
            formarTexto("6");
          }}
          variant="contained"
          size="large"
        >
          6
        </Button>
      </Grid>
      <Grid
        container
        spacing={2}
        style={{ textAlign: "center" }}
        
      >
        <Button
          onClick={() => {
            formarTexto("1");
          }}
          variant="contained"
          size="large"
        >
          1
        </Button>
        <Button
          onClick={() => {
            formarTexto("2");
          }}
          variant="contained"
          size="large"
        >
          2
        </Button>
        <Button
          onClick={() => {
            formarTexto("3");
          }}
          variant="contained"
          size="large"
        >
          3
        </Button>
      </Grid>
      <Grid
        container
        spacing={2}
        style={{ textAlign: "center" }}
        
      >
        <Button
          onClick={() => {
            formarTexto("0");
          }}
          variant="contained"
          size="large"
        >
          0
        </Button>
        <Button
          onClick={() => {
            formarTexto("");
          }}
          variant="contained"
          color="primary"
          size="large"
        >
          Limpiar
        </Button>
      </Grid>
    </CardContent>
  );
};

export default NumericKeyboard;
