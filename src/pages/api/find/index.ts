import { promises as fsPromises } from 'fs'
import chokidar from 'chokidar'
import fs from 'fs'
import parser from 'xml2json'
import { spawn } from 'child_process'
import getHandler from '@/pages/api/getHandler'
import { convertJSON } from './util'

const folder = 'C:\\BootDrv\\Aloha\\XCHECK\\20220211\\'
//const folder = '\\\\ALOHABOH\\BootDrv\\Aloha\\XCHECK\\20220211\\'
const folderFile = './src/pages/api/find/'
const logFile = folderFile + 'log.txt'
const route = 'C:\\pcnub\\Luisda.bat'

const watcher = chokidar.watch(folder, {
  ignored: /^\./,
  persistent: false,
  ignoreInitial: true,
})

const verify = (checkNumber: string, bat: string) => {
  const file = fs.readFile(
    `${folder}${checkNumber}.xml`,
    (err, data) => {
	const json = JSON.parse(parser.toJson(data, { reversible: false }))
	const validate = json.Transacction.Tenders.Tender.TenderName === 'PMS'
	console.log(validate)
	if (validate)
	  fsPromises.writeFile(route, bat).then(() => {
		spawn('cmd.exe', ['/c', route])
	  })
    },
  )
}

const run = (checkNumber: string) => {
  const bat =
    'start "" "C:\\pcnub\\Luisda.lnk" http://localhost:3000/' + checkNumber
  verify(checkNumber, bat)
}

const writeLog = (file: string, checkNumber: string) => {
  fsPromises.writeFile(file, ``).then(() => {
    run(checkNumber)
  })
}

const readLog = (checkNumber: string) => {
  const file = `${folderFile}${checkNumber}.txt`
  fsPromises
    .readFile(file, 'utf8')
    .then((data) => {
      //
    })
    .catch(function (error) {
      writeLog(file, checkNumber)
    })
}

const handler = getHandler()

watcher.on('add', function (path) {
  const checkNumber = path.replace(folder, '').replace('.xml', '')
  console.log(checkNumber)
  readLog(checkNumber)
})

handler.get((req, res) => {
  res.status(200).json({ message: 'ok' })
})

export default handler
