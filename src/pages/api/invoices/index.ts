import { Pool } from 'pg'
import fs from 'fs'
import parser from 'xml2json'
import getHandler from '@/pages/api/getHandler'
import { convertJSON } from './util'
import { print,sendPrint } from './print'

const folder = 'C:\\BootDrv\\Aloha\\XCHECK\\20220211\\'
//const folder = '\\\\BootDrv\\Aloha\\XCHECK\\'


const pool = new Pool({
  user: 'diamond',
  host: '192.168.1.5',
  database: 'hotel',
  password: 'lksdfgj53fd',
  port: 5432,
})

const handler = getHandler()

handler.get(async (req, res) => {

  const response = await pool.query('SELECT * FROM master.tbl_consumos')
  const invoices = response.rows.map((invoice) => {
    return {
      ...invoice,
      price: [invoice.price],
    }
  })
  
  res.status(200).json(invoices)
})
.post( (req, res) => {
  const { checkNumber, room, user } = req.body
  const file = fs.readFile(
    `${folder}${checkNumber}.xml`,
    (err, data) => {
      
      const json = JSON.parse(parser.toJson(data, { reversible: false }))
      const items = convertJSON(json, room)
      //sendPrint()
      print(items, user)
      
      items.forEach((item: any) => {
        const { pos, name, date, account, product, price, hab } = item
        const newQuery = `INSERT INTO master.tbl_consumos(pos, name, date, account, product, price, hab)VALUES('${pos}', '${name}', '${date}', '${account}', '${product}', ${price}, '${hab}')`
        pool.query(newQuery, (err, res) => {
         // console.log(err, res)
        })
      })
      
      
      res.status(200).json(items)
    },
  )
})

export default handler
