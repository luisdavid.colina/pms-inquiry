const Template = () => {
  return (
    <div
      className="wrapper nav-collapsed menu-collapsed"
      id="app"
      style={{ height: '1000px' }}
    >
      <div
        data-active-color="white"
        data-background-color="man-of-steel"
        data-image="img/sidebar-bg/01.jpg"
        className="app-sidebar"
        id="app"
      >
        <div className="sidebar-header">
          <div className="logo clearfix">
            <div className="logo-img">
              <img src="/logo.png" />
            </div>
            <span className="text align-middle" style={{ fontSize: '12px' }}>
              DIAMOND PMS
            </span>

            <a
              id="sidebarToggle"
              href="javascript:;"
              className="nav-toggle d-none d-sm-none d-md-none d-lg-block"
            >
              <i
                data-toggle="expanded"
                className="ft-toggle-right toggle-icon"
              ></i>
            </a>
            <a
              id="sidebarClose"
              href="javascript:;"
              className="nav-close d-block d-md-block d-lg-none d-xl-none"
            >
              <i className="ft-x"></i>
            </a>
          </div>
        </div>
        <div className="sidebar-content" >
          <div className="nav-container">
            <ul
              id="main-menu-navigation"
              data-menu="menu-navigation"
              className="navigation navigation-main"
            >
              <li className="has-sub nav-item">
                <i className="ft-grid"></i>
                <span data-i18n="" className="menu-title">
                  {' '}
                  Estados{' '}
                </span>
              </li>

              <li className="has-sub nav-item">
                <a href="#">
                  <i className="ft-phone-outgoing"></i>
                  <span data-i18n="" className="menu-title">
                    Reservas
                  </span>
                </a>
                <ul className="menu-content">
                  <li>
                    <i className="ft-phone-outgoing mr-2"></i>Menu
                  </li>
                  <li>
                    <i className="ft-sliders mr-2"></i>Filtradas
                  </li>
                  <li>
                    <i className="ft-upload-cloud mr-2"></i>importar
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div className="sidebar-background"></div>
      </div>
    </div>
  )
}

export default Template
