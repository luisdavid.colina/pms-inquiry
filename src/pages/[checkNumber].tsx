import {
  Card,
  CardContent,
  CircularProgress,
  Grid,
  TextField,
} from '@material-ui/core'
import MuiButton from '@material-ui/core/Button'
import React, { useState } from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import NumericKeyboard from '@/components/NumericKeyboard'

const Button = (props: any) => {
  return (
    <Grid item>
      <MuiButton {...props} />
    </Grid>
  )
}

export default function Home() {
  const router = useRouter()
  const checkNumber = router.query.checkNumber
  const [user, setUser] = useState('')
  const [roomInput, setRoomInput] = useState('')
  const [isSubmitting, setIsSubmitting] = useState(false)
const [search, setSearch] = useState(true)

  function formarTexto(number: string) {
    setRoomInput(number === '' ? number : roomInput + number)
  }

  const handleDownload = async () => {
    setIsSubmitting(true)
    try {
      const response = await axios.get(`/api/rooms/${roomInput}`,)
      setUser(
        response.data[0].nombre_cliente +
          ' ' +
          response.data[0].apellido_cliente,
      )
	  setSearch(false)
    } catch (error) {
      /* eslint no-console: ["error", { allow: ["error"] }] */
      setUser('Habitación vacía')
    } finally {
      setIsSubmitting(false)
    }
  }
  const onClose = async () => {
    const url = `/api/invoices`
    const req = {
      checkNumber,
      room: roomInput,
      user
    }
    await axios.post(url, req)
    window.opener = null
    window.open('', '_self')
    window.close()
  }

  return (
    <Card>
      <Grid container alignContent={'center'}>
        <Grid item xs={8}>
          <CardContent>
            <form>
              <Grid container direction="column" spacing={2}>
                <Grid item>
                  <TextField
                    InputLabelProps={{ shrink: true }}
                    disabled
                    fullWidth
                    name="room"
                    value={checkNumber}
                    label="Check Number"
                  />
                </Grid>
                <Grid item>
                  <TextField
                  InputLabelProps={{ shrink: true }}
                    disabled
                    fullWidth
                    name="room"
                    value={roomInput}
                    label="Habitación"
                  />
                </Grid>
                <Grid item>
                  <TextField
                  InputLabelProps={{ shrink: true }}
                    disabled
                    fullWidth
                    name="fullName"
                    label="Nombre"
                    value={user}
                  />
                </Grid>
                <Grid container spacing={1}>
                  <Grid item xs={6}>
                    <Button
                      fullWidth
                      disabled={isSubmitting}
                      onClick={handleDownload}
                      variant="contained"
                      color="primary"
                      startIcon={
                        isSubmitting ? (
                          <CircularProgress size="0.9rem" />
                        ) : undefined
                      }
                    >
                      {isSubmitting ? 'Buscando' : 'Buscar'}
                    </Button>
                  </Grid>
                  <Grid item xs={6}>
                    <Button
                      fullWidth
                      disabled={isSubmitting  || search }
                      onClick={onClose}
                      variant="contained"
                      startIcon={
                        isSubmitting ? (
                          <CircularProgress size="0.9rem" />
                        ) : undefined
                      }
                    >
                      {isSubmitting ? 'Guardando' : 'Guardar'}
                    </Button>
                  </Grid>
                </Grid>
              </Grid>
            </form>
          </CardContent>
        </Grid>
        <Grid item xs={4}>
          <NumericKeyboard formarTexto={formarTexto} />
        </Grid>
      </Grid>
    </Card>
  )
}
